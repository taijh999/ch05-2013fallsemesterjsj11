/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demo1028;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");//
            String url = "jdbc:mysql://localhost:3306/st2";
            conn = DriverManager.getConnection(url, "root", "root");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}