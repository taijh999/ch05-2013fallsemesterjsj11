/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demo1022;

import java.sql.*;

public class DeleteDemo {

    public static void main(String args[]) {
        Connection conn = null;
       PreparedStatement pstat = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");//
            String url = "jdbc:mysql://localhost:3306/st";
            conn = DriverManager.getConnection(url, "root", "");
            pstat = conn.prepareStatement("delete from students where no=?");
            pstat.setString(1,"199909");
            int count = pstat.executeUpdate();
            System.out.println(count);
            if (count > 0) {
                System.out.println("删除成功");
            } else {
                System.out.println("删除失败");
            }

        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }
    }
}
